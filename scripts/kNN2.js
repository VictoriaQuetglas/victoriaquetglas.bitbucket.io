//var Dataset=[];
var cantClases =2; //almacena la cantidad de clases
var DatasetTest =[];
var DatasetTraining=[];
var porcentaje = 20; //porcentaje de elementos a eliminar

function tratarData(Dataset,k) {
    Dataset.shift();//remueve la cabecera del dataset
    //Dataset.pop();//remueve el ultimo registro
    var n =Dataset.length; //cantidad de registros
    var arrayClases=new Array(cantClases); //crea un array de arrays segun clases existan [[registros C1].....[registros Cn]]
    for (let i = 1; i < (cantClases+1); i++) {
        arrayClases[i-1]=Dataset.filter(element => element.Clase=='C'+i);
    }
    var DeleteTotal = Math.round(n*(porcentaje/100)); //calcula la cantidad de elementos que va a eliminar
    var DeleteXClase = Math.trunc(DeleteTotal/cantClases); //calcula la cantidad de elementos a eliminar de cada clase
    var sobrante = DeleteTotal-(cantClases*DeleteXClase);//sobrante de la division, caso no sea exacta

    if (sobrante>0) { //tratar caso la division no sea exacta
        var ranuras=ruleta(arrayClases,n);//array de ranuras de clases  
        for (let i = 0; i < sobrante; i++) {
            var j =Math.trunc(Math.random()*99);
            var claseToDelete = ranuras[j];  
            
            for (let z = 0; z < cantClases; z++) {
                const element = arrayClases[z][0];
                if (element.Clase==claseToDelete) {
                    var h = Math.trunc(Math.random()*(arrayClases[z].length-1));//posicion del elemento a eliminar calculada al azar
                    DatasetTest.push(arrayClases[z][h]); //almacenamos el elemento eliminado en trainig
                    arrayClases[z].splice(h,1);  //se elemina el elemento
                }
                
            }
        }
    } 
    //-------------se eliminan registros que se cargaran en el dataset Prueba------------------
    for (let i = 0; i < arrayClases.length; i++) { //recorre el array clases para eliminar los elementos
        for (let z = DeleteXClase; z > 0; z--) { //elimina la cantidad de elementos indicado por DeleteClase
            var j = Math.trunc(Math.random()*(arrayClases[i].length-1));//posicion del elemento a eliminar calculada al azar
            DatasetTest.push(arrayClases[i][j]); //almacenamos el elemento eliminado en trainig
            arrayClases[i].splice(j,1);  //se elemina el elemento
        }   
    }
    arrayClases.forEach(element=>element.forEach(element2=> DatasetTraining.push(element2)));//se carga el array entrenamiento con el resto del contenido de array clases
    
    var arreglo = kOptimo(DatasetTest,DatasetTraining);
    if (k === 0) {
        k=12
    }
    var arreglo = kNN(DatasetTest,DatasetTraining);
    console.log(k)
    plotPoints(Dataset,parseInt(k))
    document.querySelector("#cargando").classList.add("d-none")
    console.log(arreglo);   
    //var x=4.28;
    //var y=5.78;
    //point=new Point(parseFloat(x), parseFloat(y));
    //var claseWin=PointClassifier(point,4,Dataset);
    //console.log(claseWin);
    
}

function kNN(DatasetTest,DatasetTraining) {
    
    arrayPrecisiones=[];
    for (let k = 1; k < 11; k++){
        var arrayPrueba=[];
        arrayPrueba.push(k);
        var precision=0;
        DatasetTest.forEach(element=>{
            var arrayDistance = calcularDistancia(element,DatasetTraining);
            var claseWin = seleccionarClase(arrayDistance,k);
            var X=element.X;
            var Y=element.Y;
            var C=element.Clase;
            var a=[X,Y,C,claseWin]
            arrayPrueba.push(a);
            if (claseWin==element.Clase) {
                precision++;
            } 
        });
        console.log(arrayPrueba);
        precision=precision/DatasetTest.length;
        var aux = [k,precision];
        arrayPrecisiones.push(aux);
    }
    
    return arrayPrecisiones;
}

function calcularDistancia(element,DatasetTraining) { //calcula la distancia del elemento i con los elementos del Training
    var arrayDistance = []; 

    for (let i = 0; i < DatasetTraining.length; i++) {
        var PuntoTraining = DatasetTraining[i];
        var distancia = Math.sqrt(Math.pow((PuntoTraining.X-element.X),2)+Math.pow((PuntoTraining.Y-element.Y),2));
        var array1 =[distancia,PuntoTraining.Clase,PuntoTraining.X,PuntoTraining.Y]; //[distancia,clase]
        arrayDistance.push(array1);
    }
    
    arrayDistance=arrayDistance.sort( function(a, b) { //ordena de menor a mayor
        if (a[0] < b[0]) return -1;
        if (a[0] > b[0]) return 1;
        return 0;
    });
    return arrayDistance;   
}

function agregarElementos(){ 
    var lista=document.getElementById("ulListado"); 
    Dato.forEach(function(data,index){
    var linew= document.createElement("li");    
    var contenido = document.createTextNode(data);
    lista.appendChild(linew);
    linew.appendChild(contenido);
    
    })
}
function ruleta(arrayClases,n) {
    var ranuras=[];

    for (let i = 1; i < (cantClases+1); i++) { //arma un arreglo de 0 a 99 con las ranuras de clases
        var cant = Math.round((arrayClases[i-1].length/n)*100);
        var clase=arrayClases[i-1][0].Clase
        
        for (let j = 0; j < cant; j++) {
            ranuras.push(clase);
            
        }
        
    } 
    return ranuras
}

function seleccionarClase(arrayDistance,k) {
    var arrayC = new Array(cantClases); //arreglo que acumula la cantidad de clases segun k 
    for (let j = 1; j < (cantClases+1); j++) { //acumula la cantidad de clases encontradas segun k
        array2 = new Array(2); //[cant,Clase]
        array2[1]='C'+j;
        array2[0]=0;

        for (let i = 0; i < k; i++) {
            const element = arrayDistance[i];
            if (element[1]=='C'+j) {
                array2[0]++; 
            }   
           arrayC[j-1]=array2;
        }
                
    }
        
    arrayC=arrayC.sort( function(a, b) { //ordena de mayor a menor las cantidades
        if (a[0] < b[0]) return 1;
        if (a[0] > b[0]) return -1;
        return 0;
    });

    var mayor = arrayC[0]; //tomo el mayor y compara si hay otras clases con la misma cantidad
    var tot=arrayC.length;
    for (let i = 1; i < tot; i++) {//recorro arrayC
        const element = arrayC[i];
        if (mayor[0]>element[0]) {
            arrayC.splice(i,1);
        } 
    };

    if (arrayC.length==1) { //caso que una sola clase es la mayor
        var claseWin = arrayC[0][1];  
    } else { //caso varias clases tienen la misma cantidad
        var ranuras=ruleta2(arrayC);//array de ranuras para tirar la ruleta
        var j =Math.trunc(Math.random()*(ranuras.length-1));//seleciona la clase ganadora por ruleta
        var claseWin = ranuras[j];
    };

    return claseWin;

}

function ruleta2(arrayC) {
    var ranuras=[];
    var n=0;
    arrayC.forEach(element=>n+=element[0]);
    for (let i = 0; i < cantClases; i++) { 
        var cant = Math.round((arrayC[i][0]/n)*100);
        var clase=arrayC[i][1]        
        for (let j = 0; j < cant; j++) {
            ranuras.push(clase);            
        }        
    } 
    return ranuras
}

function kOptimo(DatasetTest,DatasetTraining) {  //algoritmo knn para encontrar K optimo
    
    arrayPrecisiones=[];
    var n=DatasetTraining.length;

    for (let k = 1; k < (n+1); k++){
        //var arrayPrueba=[];
        //arrayPrueba.push(k);
        var precision=0;
        DatasetTest.forEach(element=>{
            var arrayDistance = calcularDistancia(element,DatasetTraining);
            var claseWin = seleccionarClase(arrayDistance,k);
            //var X=element.X;
            //var Y=element.Y;
            //var C=element.Clase;
            //var a=[X,Y,C,claseWin]
            //arrayPrueba.push(a);
            if (claseWin==element.Clase) {
                precision++;
            } 
        });
        //console.log(arrayPrueba);
        precision=precision/DatasetTest.length;
        precision=precision.toFixed(4);
        var aux = [k,precision];
        arrayPrecisiones.push(aux);
    }
    
    return arrayPrecisiones;
}

function PointClassifier(point,k,Dataset) { //Clasificador de un punto especifico
    var arrayDistance = calcularDistancia(point,Dataset);
    console.log(arrayDistance);
    var claseWin = seleccionarClase(arrayDistance,k);
    return claseWin;
}
/*
function seleccionarClase(arrayDistance,k) {
    var array2 = new Array(cantClases); //arreglo que acumula la cantidad de clases segun k 
    for (let i = 0; i < k; i++) {
        const element = arrayDistance[i];
        
        for (let j = 1; j < (cantClases+1); j++) { //acumula la cantidad de clases encontradas segun k
            if (element[1]=='C'+j) {
                if (isNaN(array2[j-1])) {
                    array2[j-1]=1;
                }else{
                    array2[j-1]++;
                }   
            }            
        }
    };

    var arrayC =[];//arreglo de arreglos [[cantC1,C1].....]
    for (let i = 0; i < array2.length; i++) {
        const element = array2[i];
        if (isNaN(array2[i])) {
        }else{
            var aux= [element,'C'+(i+1)];
            arrayC.push(aux);
        }
    };

    arrayC=arrayC.sort( function(a, b) { //ordena de mayor a menor las cantidades
        if (a[0] < b[0]) return 1;
        if (a[0] > b[0]) return -1;
        return 0;
    });

    var mayor = arrayC[0]; //tomo el mayor y compara si hay otras clases con la misma cantidad
    for (let i = 1; i < arrayC.length; i++) {
        const element = arrayC[i];
        if (mayor[0]>=element[0]) {
            arrayC.splice(i,1);
        } 
    };

    if (arrayC.length==1) {
        var claseWin = arrayC[0][1];
    } else {
        var j = Math.floor(Math.random()*arrayC.length);//posicion del elemento a eliminar calculada al azar
        var claseWin = arrayC[j][1];
    };

    return claseWin;

}
*/